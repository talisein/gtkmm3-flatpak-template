# gtkmm3-flatpak-template

A starting point for a gtkmm3 flatpak application.

## Getting started

* Clone this project
* Change the application id to a domain you control, or register with [drey.app](https://drey.app)
  * You'll need to change build-aux/app.drey.Gtkmm3Template as well as src/main.cc
* Build
  * Local builds
    1. `# meson build/`
    2. `# ninja -C build`
    3. `# build/src/gtkmm3-template`
  * Flatpak build
    1. Push your commit. Update `build-aux/app.drey.Gtkmm3Template` so that the final module points to your git repo.
    2. `# flatpak-builder flatpak-build-dir build-aux/app.drey.Gtkmm3Template.yml`
    3. `# flatpak-builder --user --install --force-clean flatpak-build-dir build-aux/app.drey.Gtkmm3Template.yml`
    4. `# flatpak run app.drey.Gtkmm3Template`
